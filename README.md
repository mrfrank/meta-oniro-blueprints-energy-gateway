<!--
SPDX-FileCopyrightText: Huawei Inc.

SPDX-License-Identifier: CC-BY-4.0
-->

# meta-oniro-blueprints-energy-gateway

Welcome to the Oniro blueprint for an Residential Energy Gateway.

Oniro is an Eclipse Foundation project focused on the development of a
distributed open source operating system for consumer devices.

*\*Oniro is a trademark of Eclipse Foundation.*

## Set up your workspace
The following instructions shows how to set up the Oniro Project workspace, please visit the [Oniro docs](https://docs.oniroproject.org/en/latest/oniro/oniro-quick-build.html) for more details.
```console
$ mkdir ~/oniroproject; cd ~/oniroproject
$ repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b kirkstone
$ repo sync --no-clone-bundle
$ TEMPLATECONF=../oniro/flavours/linux . ./oe-core/oe-init-build-env build-oniro-linux
```

## Add the Energy Gateway blueprint layers to your build
Clone this repository into your workspace
```console
$ git clone https://gitlab.eclipse.org/eclipse/oniro-blueprints/energy-gateway/meta-oniro-blueprints-energy-gateway.git ~/oniroproject/meta-oniro-blueprints-energy-gateway
```

Add the layers to your yocto build
```console
$ bitbake-layers add-layer ~/oniroproject/meta-oniro-blueprints-energy-gateway/meta-oniro-blueprints-core
$ bitbake-layers add-layer ~/oniroproject/meta-oniro-blueprints-energy-gateway/meta-oniro-blueprints-energy-gateway
```

## Build and run a linux target

Build the Oniro image with the EDDIE blueprint 
```console
$ DISTRO="oniro-linux-blueprint-energy-gateway" MACHINE=<your board> bitbake blueprint-energy-gateway-image
```
