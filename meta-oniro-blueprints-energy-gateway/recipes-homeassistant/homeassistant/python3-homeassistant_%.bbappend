# SPDX-FileCopyrightText: 2021 Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI:append = " \
                  file://auth \
                  file://auth_provider.homeassistant \
                  file://core.analytics \
                  file://core.area_registry \
                  file://core.config \
                  file://core.entity_registry \
                  file://core.restore_state \
                  file://energy \
                  file://http \
                  file://http.auth \
                  file://onboarding \
                  file://person \
                 "

HOMEASSISTANT_TRUSTED_NETWORK ?= "192.168.2.0/24"

require recipes-homeassistant/homeassistant/homeassistant-config.inc
require recipes-homeassistant/homeassistant/homeassistant-useradd.inc

do_install:append() {
    install -d "${D}${HOMEASSISTANT_CONFIG_DIR}"

    cat >> "${D}${HOMEASSISTANT_CONFIG_DIR}/configuration.yaml" << EOF

homeassistant:
  name: Residential Energy gateway
  temperature_unit: C
  unit_system: metric
  latitude: 45.464664
  longitude: 9.188540
  elevation: 120
  time_zone: Europe/Rome

  auth_providers:
    - type: trusted_networks
      trusted_networks:
        - "${HOMEASSISTANT_TRUSTED_NETWORK}"
        - 127.0.0.1
        - ::1
      allow_bypass_login: true
    - type: homeassistant

template:
  - trigger:
      - platform: time_pattern
        minutes: "/2"
    sensor:
      - name: "Washing Machine Power"
        icon: mdi:solar-power
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "0",
          "0.6",
          "0.9"
          ] | random }}
      - name: "Meter Consumed"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "Meter Returned"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "Battery In"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "Battery Out"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "PV Power"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "TV Power"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "Heat Pump Power"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "Dish Washer Power"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}
      - name: "Lights Bedroom Power"
        unit_of_measurement: kWh
        device_class: energy
        state_class: total_increasing
        state: >
          {{ [
          "1",
          "3",
          "5"
          ] | random }}

http:
  server_port: 80

energy:
frontend:

EOF

    install -d ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/auth ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/auth_provider.homeassistant ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/core.analytics ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/core.area_registry ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/core.config ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/core.entity_registry ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/core.restore_state ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/energy ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/http ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/http.auth ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/onboarding ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/
    install -m 0644 ${WORKDIR}/person ${D}${HOMEASSISTANT_CONFIG_DIR}/.storage/

    chown -R "${HOMEASSISTANT_USER}":homeassistant "${D}${HOMEASSISTANT_CONFIG_DIR}"
}

FILES_${PN} += "${HOMEASSISTANT_CONFIG_DIR}"
