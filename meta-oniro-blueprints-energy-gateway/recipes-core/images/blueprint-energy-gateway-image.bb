# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

require recipes-core/images/oniro-image-base.bb

SUMMARY = "Gateway blueprint image"
DESCRIPTION = "Residential Energy Gateway"
LICENSE = "Apache-2.0"

IMAGE_INSTALL:append = "\
    networkmanager-softap-config \
    matter \
    ot-br-posix \
    iptables \
    tayga \
    libmodbus \
    ipset \
    python3-homeassistant \
    "
